package Datos;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Connection {
    private File file;
    private FileWriter fileWriter;
    private FileReader fileReader;

    public Connection(File file) {
        this.file = file;
    }

    public void openConnectionReader(File file) throws FileNotFoundException {
        fileReader = new FileReader(file);
    }

    public void openConnectionWriter(File file) throws IOException {
        fileWriter = new FileWriter(file, true);
    }

    public FileReader getConnectionReader() throws FileNotFoundException {
        if(fileReader == null){
            openConnectionReader(file);
        }
        return fileReader;
    }

    public FileWriter getConnectionWriter(File file) throws IOException {
        if(fileWriter == null){
            openConnectionWriter(file);
        }
        return fileWriter;
    }

    public void close() throws IOException {
        if(fileReader != null){
            fileReader.close();
        }
        if(fileWriter != null){
            fileWriter.close();
        }
    }
    
}
