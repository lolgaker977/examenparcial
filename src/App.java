import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import Datos.Connection;

public class App {
    public static void main(String[] args) {
        Palabra[] palabras;
        Set<String> lista = new HashSet<>();
        int[] frecuencias;
        final String FILENAME = "examen1.txt";
        final String FILENAME2 = "respuesta.txt";
        File file2 = new File(FILENAME2);
        File file = new File(FILENAME);
        Connection connection = new Connection(file);
        ArrayList<String> listaPalabras = new ArrayList<>();

        try {
            leer(connection, listaPalabras);
            connection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        contarPalabras(listaPalabras, lista);
        palabras = new Palabra[lista.size()];

        int i = 0;
        for (Object object : lista) {
            palabras[i] = new Palabra(lista.toString());
            i++;
        }

        contarFrecuencia(listaPalabras, palabras);
        frecuencias = new int[palabras.length];

        for (int j = 0; j < palabras.length; i++) {
            frecuencias[j] = palabras[j].getFrecuencia();
        }
        


        //Ordena las frecuencias en orden descendente
        int aux;
        for (int j = 0; j < (frecuencias.length - 1); j++) {
            for (int n = 0; n < (frecuencias.length - 1); n++) {
                if (frecuencias[n] < frecuencias[n + 1]) {
                    aux = frecuencias[n];
                    frecuencias[n] = frecuencias[n + 1];
                    frecuencias[n + 1] = aux;
                }
            }
        } 
        
        

        try {
            escribir(connection, palabras, frecuencias, file2);
            connection.close();
        } catch (IOException e) {
            System.out.println("Error al escribir el resultado");
        }

    }

    //lee el texto del archivo y guarda las palabras en un array
    public static void leer(Connection c, ArrayList<String> a) throws IOException {
        FileReader fr = c.getConnectionReader();
        String texto = "";
        int codigo;
        while ((codigo = fr.read()) != -1) {
            texto += (char) codigo;
        }

        String[] p = texto.split(" ");
        for (int i = 0; i < p.length; i++) {
            a.add(p[i]);
        }
        fr.close();
    }

    public static void contarFrecuencia(ArrayList<String> a, Palabra[] p) {
        int frecuencia = 0;
        for (int i = 0; i < p.length; i++) {
            for (int n = 0; n < a.size(); n++) {
                if (p[i].getPalabra() == a.get(n)) {
                    frecuencia++;
                }
            }
            p[i].setFrecuencia(frecuencia);
            frecuencia = 0;
        }
    }

    //cuenta las palabras totales sin duplicados con la coleccion SET
    public static void contarPalabras(ArrayList<String> a, Set<String> s) {
        for (int i = 0; i < a.size(); i++) {
            s.add(a.get(i));
        }
    }

    public static void escribir(Connection c, Palabra[] p, int[] f, File file) throws IOException {
        FileWriter fw = c.getConnectionWriter(file);

        for(int i = 0; i < f.length; i++){
            for(int n = 0; n < f.length; n++){
                if(f[i] == p[n].getFrecuencia()){
                    fw.write(p[n].getPalabra());
                }
            }
        }
        System.out.println("Escritura exitosa");
        fw.close();
    }
}
