public class Palabra {
    private String palabra;
    private int frecuencia;

    public Palabra(String palabra){
        this.palabra = palabra;
    }

    public String getPalabra(){
        return palabra;
    }

    public int getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(int frecuencia) {
        this.frecuencia = frecuencia;
    }

    
}
